## AWS EC2 and LIGHTSAIL
---

This repo contains module for automated creation (provisioning) of **n-numbers** of **EC2** and **Lightsail** instances on AWS.

---

This repo is based on Terraform Version **0.12.24**, with list/array expression capability and does not require interpolation syntax.

---


## Running the Terraform Module

To run the module, follow the 2 steps below:

Step 1 - Copy the following script into a file (base.tf) in the current working directory:

---

```bash
# define provider(s) credential variables
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

module "public_cloud_resources" {
  source = "git::https://oluropoayodele@bitbucket.org/oluropoayodele/create-ec2-and-lightsail.git"
}

output "ec2_web_server_instances" {
  description = "A list of all created EC2 web server instances"
  # the list contains key-value pairs of each instance's attributes
  value = module.public_cloud_resources.aws_ec2_web_servers
}

output "ec2_db_server_instances" {
  description = "A list of all created EC2 db server instances"
  # the list contains key-value pairs of each instance's attributes
  value = module.public_cloud_resources.aws_ec2_db_servers
}

output "lightsail_instances" {
  description = "A list of all created AWS lightsail db server instances"
  # the list contains key-value pairs of each instance's attributes
  value = module.public_cloud_resources.aws_lightsail_db_servers
}

# add more outputs as necessary or desired.


```

---



Step 2 - Execute the module from the base file (base.tf) in the current working directory by typing the following commands at the prompt
(assuming **```bash```**  with **```sudo```** access):

---


```bash

  #1) init
  sudo terraform init
  
  #2) terraform plan
  sudo TF_VAR_aws_access_key="access-key-value" \
       TF_VAR_aws_secret_key="secret-key-value" \
       TF_VAR_aws_region="aws-region-value" \
       terraform plan
                                                                                    
  #3) terraform apply
  sudo TF_VAR_aws_access_key="access-key-value" \
      TF_VAR_aws_secret_key="secret-key-value" \
      TF_VAR_aws_region="aws-region-value" \
      terraform apply
```

---